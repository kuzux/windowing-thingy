import windowing_pb2
import socket
import PIL.Image as Image
import numpy as np
import pdb
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('localhost', 5678))

# returns (width, height, byte_data)
def load_image(path, viewport_size):
    img = Image.open(path)
    img = img.convert('RGB')

    max_size = max(viewport_size[0], viewport_size[1])
    width, height = img.size
    resize_factor = max_size / max(width, height)
    width = int(width * resize_factor)
    height = int(height * resize_factor)

    img = img.resize((width, height))

    x_offset = (viewport_size[0] - width)//2
    y_offset = (viewport_size[1] - height)//2

    new_img = Image.new(img.mode, viewport_size, (0, 0, 100))
    new_img.paste(img, (x_offset, y_offset))

    arr = np.array(new_img)

    r = arr[:, :, 0]
    g = arr[:, :, 1]
    b = arr[:, :, 2]

    num_pixels = viewport_size[0] * viewport_size[1]

    r = np.reshape(r, (num_pixels, ))
    g = np.reshape(g, (num_pixels, ))
    b = np.reshape(b, (num_pixels, ))

    np_bytes = np.ravel(np.dstack((r, g, b))).astype(np.uint8).tobytes()
    return (viewport_size[0], viewport_size[1], np_bytes)

def send_cmd(cmd):
    byte_string = cmd.SerializeToString()
    length_in_bytes = len(byte_string)

    sock.send(length_in_bytes.to_bytes(4, byteorder='big'))
    sock.send(byte_string)

def recv_event():
    length_data = sock.recv(4)
    # in case the connection to the server is closed
    if length_data == b'':
        raise Exception('Connection closed to windowing server')
    length_in_bytes = int.from_bytes(length_data, byteorder='big')
    byte_string = sock.recv(length_in_bytes)
    evt = windowing_pb2.WindowingEvent()
    evt.ParseFromString(byte_string)
    return evt

cmd = windowing_pb2.WindowingCommand()
cmd.query_display_size.CopyFrom(windowing_pb2.WindowingCommand.QueryDisplaySizeCommand())
send_cmd(cmd)

def send_desktop_bg(viewport_size):
    image_info = load_image("cat1.jpg", viewport_size)
    print("got img info")

    cmd.Clear()
    print(image_info[:-1])
    cmd.resize.CopyFrom(windowing_pb2.WindowingCommand.ResizeCommand(width=image_info[0], height=image_info[1]))
    print("resize cmd", cmd)
    send_cmd(cmd)

    cmd.Clear()
    cmd.window_contents.CopyFrom(windowing_pb2.WindowingCommand.WindowContentsCommand(contents=image_info[2]))
    send_cmd(cmd)

    cmd.Clear()
    cmd.set_window_class.CopyFrom(windowing_pb2.WindowingCommand.SetWindowClassCommand(window_class=windowing_pb2.WindowingCommand.WindowClass.WINDOW_CLASS_BACKGROUND))
    send_cmd(cmd)

    cmd.Clear()
    cmd.show_hide.CopyFrom(windowing_pb2.WindowingCommand.ShowHideCommand(show=True))
    send_cmd(cmd)

while True:
    evt = None
    try:
        evt = recv_event()
    except KeyboardInterrupt:
        break
    except Exception as e:
        print('Receive error: ', str(e))
        break

    try:
        print(evt)
        if evt.WhichOneof('events') == 'close':
            break
        if evt.WhichOneof('events') == 'display_size':
            viewport_size = (evt.display_size.width, evt.display_size.height)
            send_desktop_bg(viewport_size)
    except KeyboardInterrupt:
        break
    except Exception as e:
        print('Error processing event: ', str(e))
        break
