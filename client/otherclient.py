import windowing_pb2
import socket
import PIL.Image as Image
import pdb

import time

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('localhost', 5678))

# returns (width, height, byte_data)
def load_image(path, max_size):
    img = Image.open(path)
    img = img.convert('RGB')

    width, height = img.size
    resize_factor = max_size / max(width, height)
    width = int(width * resize_factor)
    height = int(height * resize_factor)

    img = img.resize((width, height))
    byte_data = b''
    for pixel in img.getdata():
        byte_data += bytes(pixel)

    return (width, height, byte_data)

def send_cmd(cmd):
    byte_string = cmd.SerializeToString()
    length_in_bytes = len(byte_string)

    sock.send(length_in_bytes.to_bytes(4, byteorder='big'))
    sock.send(byte_string)

def recv_event():
    length_data = sock.recv(4)
    # in case the connection to the server is closed
    if length_data == b'':
        raise Exception('Connection closed to windowing server')
    length_in_bytes = int.from_bytes(length_data, byteorder='big')
    byte_string = sock.recv(length_in_bytes)
    evt = windowing_pb2.WindowingEvent()
    evt.ParseFromString(byte_string)
    return evt

image_info = load_image("cat2.jpg", 300)

cmd = windowing_pb2.WindowingCommand()

cmd.Clear()
cmd.set_title.CopyFrom(windowing_pb2.WindowingCommand.SetTitleCommand(title="zxcv"))
send_cmd(cmd)

cmd.Clear()
cmd.move.CopyFrom(windowing_pb2.WindowingCommand.MoveCommand(x=250, y=150))
send_cmd(cmd)

cmd.Clear()
cmd.resize.CopyFrom(windowing_pb2.WindowingCommand.ResizeCommand(width=image_info[0], height=image_info[1]))
send_cmd(cmd)

cmd.Clear()
cmd.set_bg_color.CopyFrom(windowing_pb2.WindowingCommand.SetBgColorCommand(r=0, g=200, b=0))
send_cmd(cmd)

cmd.Clear()
cmd.window_contents.CopyFrom(windowing_pb2.WindowingCommand.WindowContentsCommand(contents=image_info[2]))
send_cmd(cmd)

cmd.Clear()
cmd.show_hide.CopyFrom(windowing_pb2.WindowingCommand.ShowHideCommand(show=True))
send_cmd(cmd)

while True:
    try:
        evt = recv_event()
        print(evt)
        if evt.WhichOneof('events') == 'close':
            break
    except KeyboardInterrupt:
        break
    except Exception as e:
        print('Receive error: ', str(e))
        break

# pdb.set_trace()