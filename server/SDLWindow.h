#pragma once
#include <SDL.h>

#include <functional>
#include <string>
#include <string_view>
#include <unordered_map>

// #define USE_OPENGL

class SDLWindow
{
public:
    SDLWindow(std::string_view title, SDL_Point size);
    ~SDLWindow();

    void draw();
    SDL_Point window_size() const { return m_window_size; }
    void set_window_size(SDL_Point size) { m_window_size = size; }
    void set_title(std::string_view title);
    void set_icon(std::string const& filename);

    SDL_Renderer* renderer() { return m_renderer; }

private:
    SDL_Window* window = nullptr;
    SDL_Renderer* m_renderer = nullptr;
#ifdef USE_OPENGL
    SDL_GLContext ctx;
#endif

    SDL_Point m_window_size;
    SDL_Surface* m_window_icon { nullptr };
};

class SDLEventLoop
{
public:
    SDLEventLoop() { }

    void run(std::function<void(uint64_t)> body);
    void register_event(SDL_EventType type, std::function<void(SDL_Event)> callback);
    void stop();

private:
    bool m_running { false };
    std::unordered_map<SDL_EventType, std::function<void(SDL_Event)>> m_event_cbs;
};
