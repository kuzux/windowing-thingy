#pragma once

#include <poll.h>
#include <pthread.h>

#include <functional>
#include <unordered_map>
#include <vector>

class Server
{
    struct ConnectionInfo {
        int fd;
        // -1 means we haven't read the number of bytes yet
        int remaining_bytes;

        size_t buf_write_pos;
        uint8_t* buf;
    };

public:
    // we can use the file descriptor as a client id as well
    // Should I do something like a newtype to enforce this distinction in the compile time?
    using ClientId = int;

    Server(uint16_t port);
    ~Server();

    void on_new_connection(std::function<void(ClientId)> cb) { m_on_new_connection_cb = cb; }
    void on_data_received(std::function<void(ClientId, uint8_t*, size_t)> cb)
    {
        m_on_data_received_cb = cb;
    }
    void on_connection_closed(std::function<void(ClientId)> cb) { m_on_connection_closed_cb = cb; }

    // This sends the data to the connection specified by its id.
    // Returns the number of bytes sent.
    // TODO: Have a better error class for fallible stuff
    int send_data(ClientId id, uint8_t const* data, size_t len);

    void run();
    void shutdown();

private:
    static void* thread_proc(void* arg);

    // these return 0 on success, negative on failure
    int create_server_socket();
    int accept_new_connection();
    int remove_connection(ClientId id);

    // returns -1 if no fd is ready
    int find_ready_fd() const;

    // returns the number of bytes read
    int read_more_data(ClientId id);

    uint16_t m_port;
    pthread_t m_thread = nullptr;
    std::function<void(ClientId)> m_on_new_connection_cb;
    std::function<void(ClientId, uint8_t*, size_t)> m_on_data_received_cb;
    std::function<void(ClientId)> m_on_connection_closed_cb;

    int m_listen_fd = -1;
    std::vector<struct pollfd> m_pollfds;
    // fd -> Connection object
    std::unordered_map<ClientId, ConnectionInfo> m_connections;
};
