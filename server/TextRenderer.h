#pragma once

#include <string>
#include <unordered_map>

#include <SDL.h>
#include <SDL_ttf.h>

class TextRenderer
{
public:
    struct RenderedText {
        SDL_Texture* texture;
        uint32_t width;
        uint32_t height;
        uint32_t last_used;
    };

    TextRenderer() { }
    ~TextRenderer();

    int load_font(SDL_Renderer* renderer, std::string const& font_path, uint32_t font_size);

    // TODO: Make this take color as well
    RenderedText render_text(std::string const& text);

    void tick(uint32_t ticks);

private:
    uint32_t m_ticks = 0;
    SDL_Renderer* m_renderer { nullptr };
    TTF_Font* m_font { nullptr };

    std::unordered_map<std::string, RenderedText> m_text_cache;
};
