#include "WindowManager.h"

#include <SDL.h>
#include <fmt/core.h>

SDL_Rect WindowManager::Window::client_rect() const { return { (int)x, (int)y, (int)w, (int)h }; }

SDL_Rect WindowManager::Window::window_outer_rect() const
{
    auto window_contents_rect = client_rect();
    return { window_contents_rect.x - 2, window_contents_rect.y - 27, window_contents_rect.w + 4,
        window_contents_rect.h + 29 };
}

SDL_Rect WindowManager::Window::titlebar_rect() const
{
    auto window_contents_rect = client_rect();
    return { window_contents_rect.x, window_contents_rect.y - 25, window_contents_rect.w, 25 };
}

SDL_Rect WindowManager::Window::close_button_rect() const
{
    auto window_contents_rect = client_rect();
    return { window_contents_rect.x + window_contents_rect.w - 20, window_contents_rect.y - 20, 15,
        15 };
}

WindowManager::WindowId WindowManager::create_window(Server::ClientId id)
{
    Window window;
    window.client_id = id;

    window.x = 0;
    window.y = 0;
    window.w = 0;
    window.h = 0;

    window.background_color[0] = 0;
    window.background_color[1] = 0;
    window.background_color[2] = 0;

    window.shown = false;
    window.class_ = WindowClass::Normal;

    auto window_id = window_id_from_client_id(id);
    m_windows[window_id] = window;
    m_window_stack.push_back(&m_windows[window.client_id]);
    return window_id;
}

void WindowManager::destroy_window(WindowManager::WindowId id)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;

    fmt::print(stderr, "Destroying window {}\n", id);
    m_windows.erase(it);

    auto it2 = std::find(m_window_stack.begin(), m_window_stack.end(), &m_windows[id]);
    if (it2 == m_window_stack.end())
        return;

    m_window_stack.erase(it2);
}

WindowManager::Window const* WindowManager::get_window(WindowId id) const
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return nullptr;

    return &it->second;
}

void WindowManager::move_window_to(WindowId id, uint32_t x, uint32_t y)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;

    auto window = &it->second;
    fmt::print(stderr, "Moving window {} to ({}, {})\n", id, x, y);

    window->x = x;
    window->y = y;
}

void WindowManager::resize_window(WindowId id, uint32_t w, uint32_t h)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;

    auto window = &it->second;
    fmt::print(stderr, "Resizing window {} to ({}, {})\n", id, w, h);

    window->w = w;
    window->h = h;
}

void WindowManager::set_window_title(WindowId id, std::string const& title)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;

    auto window = &it->second;
    fmt::print(stderr, "Setting title of window {} to {}\n", id, title);

    window->title = title;
}

void WindowManager::set_window_background_color(WindowId id, uint8_t r, uint8_t g, uint8_t b)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;

    auto window = &it->second;
    fmt::print(stderr, "Setting background color of window {} to ({}, {}, {})\n", id, r, g, b);

    window->background_color[0] = r;
    window->background_color[1] = g;
    window->background_color[2] = b;
}

void WindowManager::set_window_shown(WindowId id, bool shown)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;

    auto window = &it->second;
    if (shown == window->shown)
        return;

    if (shown)
        fmt::print(stderr, "Showing window {}\n", id);
    else
        fmt::print(stderr, "Hiding window {}\n", id);

    window->shown = shown;
}

WindowManager::Hit WindowManager::hit_test_no_cache(uint32_t x, uint32_t y) const
{
    SDL_Point point = { (int)x, (int)y };
    for (auto it = top(); it != bottom_end(); ++it) {
        auto const& window = **it;

        auto window_id = window_id_from_client_id(window.client_id);

        SDL_Rect window_contents_rect = window.client_rect();
        SDL_Rect titlebar_rect = window.titlebar_rect();
        SDL_Rect border_rect = window.window_outer_rect();
        SDL_Rect close_button_rect = window.close_button_rect();

        if (SDL_PointInRect(&point, &close_button_rect))
            return { HitArea::CloseButton, window_id, point };
        else if (SDL_PointInRect(&point, &titlebar_rect))
            return { HitArea::TitleBar, window_id, point };
        else if (SDL_PointInRect(&point, &window_contents_rect))
            return { HitArea::ClientArea, window_id, point };

        if (SDL_PointInRect(&point, &border_rect)) {
            auto border_area = HitArea::Border;

            auto left_right = HitArea::None;
            auto top_bottom = HitArea::None;

            if (point.x < border_rect.x + 5)
                left_right = HitArea::LeftBorder;
            else if (point.x > border_rect.x + border_rect.w - 5)
                left_right = HitArea::RightBorder;

            if (point.y < border_rect.y + 5)
                top_bottom = HitArea::TopBorder;
            else if (point.y > border_rect.y + border_rect.h - 5)
                top_bottom = HitArea::BottomBorder;

            if (left_right == HitArea::LeftBorder && top_bottom == HitArea::TopBorder)
                border_area = HitArea::TopLeftCorner;
            else if (left_right == HitArea::RightBorder && top_bottom == HitArea::TopBorder)
                border_area = HitArea::TopRightCorner;
            else if (left_right == HitArea::LeftBorder && top_bottom == HitArea::BottomBorder)
                border_area = HitArea::BottomLeftCorner;
            else if (left_right == HitArea::RightBorder && top_bottom == HitArea::BottomBorder)
                border_area = HitArea::BottomRightCorner;
            else if (left_right == HitArea::None)
                border_area = top_bottom;
            else if (top_bottom == HitArea::None)
                border_area = left_right;

            return { border_area, window_id, point };
        }
    }

    return { HitArea::None, 0, point };
}

WindowManager::Hit WindowManager::hit_test(uint32_t x, uint32_t y)
{
    SDL_Point mouse_coords = { (int)x, (int)y };
    if (!m_cached_hit) {
        m_cached_hit = hit_test_no_cache(x, y);
        return *m_cached_hit;
    }
    if (m_cached_hit->point.x == x && m_cached_hit->point.y == y)
        return *m_cached_hit;
    return hit_test_no_cache(x, y);
}

void WindowManager::move_window_to_top(WindowId id)
{
    auto const* ptr_to_window = get_window(id);
    if (!ptr_to_window)
        return;
    if (ptr_to_window->class_ != WindowClass::Normal)
        return;

    fmt::print(stderr, "Moving window {} to top\n", id);

    // this moves the window to the end of the list (which represents the top)
    std::remove(m_window_stack.begin(), m_window_stack.end(), ptr_to_window);
    m_window_stack[m_window_stack.size() - 1] = ptr_to_window;
}

SDL_Texture* WindowManager::create_window_texture(WindowId id, SDL_Renderer* renderer)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return nullptr;

    auto& window = it->second;
    if (window.back_buffer && window.back_texture_width == window.w
        && window.back_texture_height == window.h)
        return window.back_buffer;

    // In case of resize, we only create a back texture
    // Next time we swap, the back texture will become the front texture
    // If we write to the back texture again (with the new size), we'll create a new texture again
    fmt::print(stderr, "Creating texture for window {}\n", id);
    if (window.back_buffer)
        SDL_DestroyTexture(window.back_buffer);
    window.back_texture_width = window.w;
    window.back_texture_height = window.h;
    window.back_buffer = SDL_CreateTexture(
        renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, window.w, window.h);
    window.back_last_written = 0;

    return window.back_buffer;
}

void WindowManager::set_window_contents(WindowId id, uint8_t const* pixels)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;
    auto& window = it->second;

    fmt::print(stderr, "Setting contents of window {}\n", id);

    void* dest_pixels = nullptr;
    int pitch = (int)window.w * 4;
    SDL_LockTexture(window.back_buffer, nullptr, (void**)&dest_pixels, &pitch);
    memcpy(dest_pixels, pixels, window.w * window.h * 4);
    SDL_UnlockTexture(window.back_buffer);
    window.back_last_written = m_ticks;
}

void WindowManager::tick(uint32_t ticks)
{
    m_ticks = ticks;
    m_cached_hit = std::nullopt;
    for (auto& [_id, window] : m_windows) {
        if (window.back_last_written <= window.front_last_written)
            continue;

        // FIXME: All these swaps should be atomic, really
        std::swap(window.back_buffer, window.front_buffer);
        std::swap(window.back_texture_width, window.front_texture_width);
        std::swap(window.back_texture_height, window.front_texture_height);
        std::swap(window.back_last_written, window.front_last_written);
    }
}

void WindowManager::set_window_class(WindowId id, WindowClass window_class)
{
    auto it = m_windows.find(id);
    if (it == m_windows.end())
        return;
    auto& window = it->second;

    fmt::print(stderr, "Setting class of window {} to {}\n", id, (int)window_class);
    window.class_ = window_class;

    if (window_class == WindowClass::Background) {
        m_window_stack.erase(std::remove(m_window_stack.begin(), m_window_stack.end(), &window));
        m_window_stack.insert(m_window_stack.begin(), &window);
    } else if (window_class == WindowClass::Normal) {
        m_window_stack.erase(std::remove(m_window_stack.begin(), m_window_stack.end(), &window));
        auto new_it = std::find_if(m_window_stack.begin(), m_window_stack.end(),
            [](auto* w) { return w->class_ == WindowClass::Background; });
        if (new_it != m_window_stack.end())
            new_it++;
        m_window_stack.insert(new_it, &window);
    }
}

void WindowManager::draw_window_onto(Window const& window, SDL_Renderer* renderer)
{
    if (!window.shown)
        return;

    SDL_Rect window_rect = { (int)window.x, (int)window.y, (int)window.w, (int)window.h };
    // TODO: Clip the window to the screen

    if (window.class_ == WindowManager::WindowClass::Normal) {
        // draw the boundary
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_Rect border_rect
            = { window_rect.x - 2, window_rect.y - 27, window_rect.w + 4, window_rect.h + 29 };
        SDL_RenderFillRect(renderer, &border_rect);

        // draw a title bar
        SDL_SetRenderDrawColor(renderer, 125, 125, 125, 255);
        SDL_Rect titlebar_rect = { window_rect.x, window_rect.y - 25, window_rect.w, 25 };
        SDL_RenderFillRect(renderer, &titlebar_rect);

        // draw the title text
        auto rendered_text = m_text_renderer.render_text(window.title);
        SDL_Rect text_rect = { (int)(window_rect.x + (window_rect.w - rendered_text.width) / 2),
            (int)(window_rect.y - rendered_text.height - 3), (int)rendered_text.width,
            (int)rendered_text.height };
        SDL_RenderCopy(renderer, rendered_text.texture, nullptr, &text_rect);

        // And the close button
        SDL_Rect close_button_rect
            = { window_rect.x + window_rect.w - 20, window_rect.y - 20, 15, 15 };
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        SDL_RenderFillRect(renderer, &close_button_rect);
    }

    // then the window contents
    if (window.front_buffer) {
        SDL_RenderCopy(renderer, window.front_buffer, nullptr, &window_rect);
    } else {
        SDL_SetRenderDrawColor(renderer, window.background_color[0], window.background_color[1],
            window.background_color[2], 255);
        SDL_RenderFillRect(renderer, &window_rect);
    }
}
