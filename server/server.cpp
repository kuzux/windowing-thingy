#include "server.h"

#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>

Server::Server(uint16_t port)
    : m_port(port)
{
}

Server::~Server()
{
    if (m_thread != nullptr) {
        std::cerr << "Server::~Server() called while server is still running, "
                     "sending SIGKILL to server thread"
                  << std::endl;
        pthread_kill(m_thread, SIGKILL);
    }
}

int Server::create_server_socket()
{
    int rc;
    struct addrinfo hints, *res;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    const char* port_str = std::to_string(m_port).c_str();

    rc = getaddrinfo("0.0.0.0", port_str, &hints, &res);
    if (rc != 0)
        return -1;

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        return -1;

    rc = bind(sockfd, (struct sockaddr*)res->ai_addr, res->ai_addrlen);
    if (rc < 0)
        return -1;

    rc = listen(sockfd, 20);
    if (rc < 0)
        return -1;

    m_listen_fd = sockfd;
    m_pollfds.push_back({ sockfd, POLLIN, 0 });

    return sockfd;
}

void Server::run()
{
    int rc = pthread_create(&m_thread, NULL, Server::thread_proc, (void*)this);
    // TODO: handle error in case pthread_create fails
    // TODO: Create multiple worker threads for executing different callbacks
    // simultaneously And all the synchronization stuff that entails
}

int Server::find_ready_fd() const
{
    for (auto& pfd : m_pollfds) {
        if (pfd.revents & POLLIN)
            return pfd.fd;
    }
    return -1;
}

int Server::accept_new_connection()
{
    if (m_listen_fd < 0)
        return -1;

    struct sockaddr_in client_addr;
    socklen_t client_addr_len = sizeof(client_addr);
    int client_fd = accept(m_listen_fd, (struct sockaddr*)&client_addr, &client_addr_len);
    if (client_fd < 0)
        return -1;
    ClientId id = client_fd;

    m_pollfds.push_back({ client_fd, POLLIN, 0 });
    uint8_t* client_buf = new uint8_t[1024 * 1024 * 8];
    m_connections[id] = { client_fd, -1, 0, client_buf };
    if (m_on_new_connection_cb)
        m_on_new_connection_cb(id);
    return 0;
}

int Server::remove_connection(ClientId id)
{
    auto it = std::find_if(
        m_pollfds.begin(), m_pollfds.end(), [&](const struct pollfd& pfd) { return pfd.fd == id; });
    if (it == m_pollfds.end())
        return -1;
    m_pollfds.erase(it);

    delete[] m_connections[id].buf;
    m_connections.erase(id);

    if (m_on_connection_closed_cb)
        m_on_connection_closed_cb(id);

    return 0;
}

int Server::read_more_data(ClientId id)
{
    auto it = m_connections.find(id);
    if (it == m_connections.end())
        return -1;

    ConnectionInfo& conn = m_connections[id];
    int fd = id;
    if (conn.remaining_bytes == -1) {
        uint32_t bytes_network_order;

        // We haven't read the number of bytes yet
        int rc = read(fd, &bytes_network_order, sizeof(bytes_network_order));
        if (rc < 0)
            return rc;
        if (rc == 0) {
            std::cout << "EOF" << std::endl;
            remove_connection(fd);
            return rc;
        }

        conn.remaining_bytes = ntohl(bytes_network_order);
        std::cout << "read remaining bytes " << conn.remaining_bytes << std::endl;
        return rc;
    }

    int rc = read(fd, conn.buf + conn.buf_write_pos, conn.remaining_bytes);
    if (rc < 0)
        return rc;
    if (rc == 0) {
        std::cout << "EOF" << std::endl;
        remove_connection(fd);
        return rc;
    }

    conn.buf_write_pos += rc;
    conn.remaining_bytes -= rc;

    if (conn.remaining_bytes == 0) {
        // We have read all the bytes
        if (m_on_data_received_cb)
            m_on_data_received_cb(id, conn.buf, conn.buf_write_pos);
        conn.remaining_bytes = -1;
    }
    return rc;
}

void* Server::thread_proc(void* arg)
{
    auto self = (Server*)arg;
    self->create_server_socket();

    if (self->m_listen_fd < 0) {
        std::cerr << "Failed to create server socket" << std::endl;
        return NULL;
    }

    std::cout << "Server listening on port " << self->m_port << std::endl;

    int poll_timeout = 3 * 60 * 1000;

    while (1) {
        int number_of_fds_ready
            = poll(self->m_pollfds.data(), self->m_pollfds.size(), poll_timeout);
        if (number_of_fds_ready < 0)
            return NULL;

        while (number_of_fds_ready > 0) {
            int fd = self->find_ready_fd();
            if (fd == self->m_listen_fd)
                self->accept_new_connection();
            else
                self->read_more_data(fd);

            number_of_fds_ready--;
        }
    }
}

int Server::send_data(ClientId id, const uint8_t* data, size_t len)
{
    if (m_connections.find(id) == m_connections.end())
        return -1;
    if (id == m_listen_fd)
        return -1;

    int fd = id;

    uint32_t len_network_order = htonl(len);
    int rc = write(fd, &len_network_order, sizeof(len_network_order));
    if (rc < 0)
        return rc;
    rc = write(fd, data, len);
    return rc;
}

void Server::shutdown()
{
    for (auto pollfd : m_pollfds) {
        if (pollfd.fd == m_listen_fd)
            continue;
        ::shutdown(pollfd.fd, SHUT_RDWR);
        ::close(pollfd.fd);
    }
    ::shutdown(m_listen_fd, SHUT_RDWR);
    ::close(m_listen_fd);
    m_pollfds.clear();
    m_listen_fd = -1;
    pthread_kill(m_thread, SIGKILL);
    m_thread = nullptr;
}
