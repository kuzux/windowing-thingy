#!/bin/bash

echo "Running clang-format..."
CF_FLAGS="--dry-run -Werror" make format 
if [[ $? -ne 0 ]]; then
    echo "clang-format failed. Auto format code? (Y/n)"
    read -r answer
    if [[ $answer == "Y" ]]; then
        echo "Auto formatting code..."
        make format
    fi
else
    echo "clang-format passed."
fi
