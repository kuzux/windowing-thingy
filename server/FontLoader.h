#pragma once

#include <fontconfig/fontconfig.h>
#include <optional>
#include <string>

class FontLoader
{
public:
    FontLoader();
    ~FontLoader();

    std::optional<std::string> locate_font(std::string const& pattern);

private:
    FcConfig* m_config { nullptr };
};