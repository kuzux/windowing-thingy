#include "FontLoader.h"
using namespace std;

FontLoader::FontLoader()
{
    m_config = FcInitLoadConfigAndFonts();
    assert(m_config);
}

FontLoader::~FontLoader() { FcConfigDestroy(m_config); }

optional<string> FontLoader::locate_font(string const& pattern)
{
    // https://www.camconn.cc/post/how-to-fontconfig-lib-c/
    FcPattern* pat = FcNameParse(reinterpret_cast<const FcChar8*>(pattern.c_str()));
    FcConfigSubstitute(m_config, pat, FcMatchPattern);
    FcDefaultSubstitute(pat);

    FcFontSet* fs = FcFontSetCreate();
    FcObjectSet* os = FcObjectSetBuild(FC_FAMILY, FC_STYLE, FC_FILE, NULL);

    FcResult result;
    FcFontSet* font_patterns = FcFontSort(m_config, pat, FcTrue, 0, &result);
    assert(font_patterns);
    if (font_patterns->nfont <= 0)
        return {};

    FcPattern* font_pattern = FcFontRenderPrepare(m_config, pat, font_patterns->fonts[0]);
    assert(font_pattern);
    FcFontSetAdd(fs, font_pattern);

    FcFontSetSortDestroy(font_patterns);
    FcPatternDestroy(pat);

    if (fs->nfont <= 0)
        return {};

    FcValue v;
    FcPattern* font;

    font = FcPatternFilter(fs->fonts[0], os);
    FcPatternGet(font, FC_FILE, 0, &v);
    string retval = (char*)v.u.f;

    FcPatternDestroy(font);
    FcObjectSetDestroy(os);
    FcFontSetDestroy(fs);

    return retval;
}