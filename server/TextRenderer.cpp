#include "TextRenderer.h"

// We need to make sure this is thread safe if we want to create text renderers on multiple threads
// Though why would we? :)
static uint32_t s_initialized_count = 0;

int TextRenderer::load_font(
    SDL_Renderer* renderer, std::string const& font_path, uint32_t font_size)
{
    if (s_initialized_count == 0)
        TTF_Init();

    m_renderer = renderer;
    m_font = TTF_OpenFont(font_path.c_str(), font_size);

    s_initialized_count++;
}

TextRenderer::~TextRenderer()
{
    if (!m_renderer)
        return;
    for (auto& [text, info] : m_text_cache)
        SDL_DestroyTexture(info.texture);
    if (m_font)
        TTF_CloseFont(m_font);
    s_initialized_count--;
    if (s_initialized_count == 0)
        TTF_Quit();
}

TextRenderer::RenderedText TextRenderer::render_text(std::string const& text)
{
    if (text.empty())
        return { nullptr, 0, 0, 0 };

    auto it = m_text_cache.find(text);
    if (it != m_text_cache.end()) {
        it->second.last_used = m_ticks;
        return it->second;
    }

    auto surface = TTF_RenderText_Solid(m_font, text.c_str(), { 255, 255, 255 });
    uint32_t width = surface->w;
    uint32_t height = surface->h;
    auto texture = SDL_CreateTextureFromSurface(m_renderer, surface);
    SDL_FreeSurface(surface);

    m_text_cache[text] = { texture, width, height, m_ticks };
    return { texture, width, height, m_ticks };
}

void TextRenderer::tick(uint32_t ticks)
{
    m_ticks = ticks;
    for (auto it = m_text_cache.begin(); it != m_text_cache.end();) {
        if (ticks - it->second.last_used > 1) {
            SDL_DestroyTexture(it->second.texture);
            it = m_text_cache.erase(it);
        } else {
            it++;
        }
    }
}
