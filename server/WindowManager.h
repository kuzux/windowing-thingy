#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include "TextRenderer.h"
#include "server.h"
#include <SDL.h>

class WindowManager
{
public:
    using WindowId = Server::ClientId;

    enum class WindowClass { Normal, Background, Panel };

    struct Window {
        Server::ClientId client_id;

        uint32_t x, y, w, h;
        std::string title;

        // red green blue
        uint8_t background_color[3];

        // This has double buffering, a front and a back texture
        // Not doing it causes a crash if we try to render at the same time as updating the texture
        // TODO: I think having a separate window texture class (doing the double buffering logic)
        // would be better
        SDL_Texture* front_buffer { nullptr };
        SDL_Texture* back_buffer { nullptr };

        // These are useful for creating a new texture on resize
        uint32_t front_texture_width;
        uint32_t front_texture_height;

        uint32_t back_texture_width;
        uint32_t back_texture_height;

        // These are used when deciding whether we should swap the buffers or not
        uint32_t front_last_written = 0;
        uint32_t back_last_written = 0;

        bool shown;
        WindowClass class_;

        SDL_Rect client_rect() const;
        SDL_Rect window_outer_rect() const;
        SDL_Rect titlebar_rect() const;
        SDL_Rect close_button_rect() const;
    };

    WindowManager(TextRenderer& text_renderer)
        : m_text_renderer(text_renderer)
    {
    }
    ~WindowManager() { }

    void tick(uint32_t ticks);

    Window const* get_window(WindowId id) const;
    WindowId window_id_from_client_id(Server::ClientId client_id) const { return client_id; }

    WindowId create_window(Server::ClientId client_id);
    void destroy_window(WindowId id);

    void move_window_to(WindowId id, uint32_t x, uint32_t y);
    void resize_window(WindowId id, uint32_t w, uint32_t h);
    void set_window_title(WindowId id, std::string const& title);
    void set_window_background_color(WindowId id, uint8_t r, uint8_t g, uint8_t b);
    void set_window_shown(WindowId id, bool shown);
    void set_window_class(WindowId id, WindowClass window_class);

    SDL_Texture* create_window_texture(WindowId id, SDL_Renderer* renderer);
    void set_window_contents(WindowId id, uint8_t const* pixels);

    std::vector<Window const*>::const_iterator bottom() const { return m_window_stack.begin(); }
    std::vector<Window const*>::const_reverse_iterator top() const
    {
        return m_window_stack.rbegin();
    }

    std::vector<Window const*>::const_iterator top_end() const { return m_window_stack.end(); }
    std::vector<Window const*>::const_reverse_iterator bottom_end() const
    {
        return m_window_stack.rend();
    }

    enum class HitArea {
        None,
        TitleBar,
        CloseButton,
        Border,
        LeftBorder,
        RightBorder,
        BottomBorder,
        TopBorder,
        BottomLeftCorner,
        BottomRightCorner,
        TopLeftCorner,
        TopRightCorner,
        ClientArea
    };

    struct Hit {
        HitArea area;
        WindowId window_id;
        SDL_Point point;
    };

    Hit hit_test(uint32_t x, uint32_t y);

    void move_window_to_top(WindowId id);

    /*
    void move_window_up(WindowId id);
    void move_window_down(WindowId id);

    void move_window_to_bottom(WindowId id);
    */

    // void focus_window(WindowId id);

    void draw_window_onto(Window const& window, SDL_Renderer* renderer);

private:
    Hit hit_test_no_cache(uint32_t x, uint32_t y) const;

    uint32_t m_ticks = 0;

    // TODO: These need to be synchronized using a mutex
    std::unordered_map<WindowId, Window> m_windows;

    // we can do this since references to objects in an unordered_map are stable
    std::vector<Window const*> m_window_stack;

    // We have a cache here (of hit test results) and clear it after every tick
    // By the way, I think we can assume every frame will have a single hit test location
    std::optional<Hit> m_cached_hit = std::nullopt;

    // std::optional<WindowId> m_focused_window = std::nullopt;
    TextRenderer& m_text_renderer;
};