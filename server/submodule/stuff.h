#pragma once

namespace Submodule
{
class Stuff
{
public:
    Stuff();
    ~Stuff();
    void do_stuff();
};
}