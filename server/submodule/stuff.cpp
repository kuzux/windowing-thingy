#include <submodule/stuff.h>

#include <fmt/color.h>
#include <fmt/core.h>

namespace Submodule
{

Stuff::Stuff() { fmt::print(fmt::fg(fmt::color::green), "Stuff::Stuff()\n"); }

Stuff::~Stuff() { fmt::print(fmt::fg(fmt::color::red), "Stuff::~Stuff()\n"); }

void Stuff::do_stuff() { fmt::print(fmt::fg(fmt::color::yellow), "Stuff::do_stuff()\n"); }

}