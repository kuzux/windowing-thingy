#include <SDL.h>
#include <SDL_ttf.h>
#include <fmt/format.h>
#include <signal.h>
#include <submodule/stuff.h>
#include <vendor/windowing.pb.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "FontLoader.h"
#include "SDLWindow.h"
#include "TextRenderer.h"
#include "WindowManager.h"
#include "server.h"

using namespace std;

void send_display_size(Server& server, Server::ClientId id)
{
    WindowingEvent event;
    auto* display_size_event = event.mutable_display_size();
    // TODO: Don't hardcode viewport size so we don't need to send hardcoded
    // values to the client
    display_size_event->set_width(800);
    display_size_event->set_height(600);

    uint8_t buf[1024];
    size_t len = event.ByteSizeLong();
    event.SerializeToArray(buf, len);

    server.send_data(id, buf, len);
}

// TODO: This should probably be a service object
void handle_windowing_command(WindowManager& manager, SDLWindow& main_window, Server& server,
    Server::ClientId id, uint8_t* buf, size_t len)
{
    WindowingCommand cmd;
    cmd.ParseFromArray(buf, len);

    auto window_id = manager.window_id_from_client_id(id);
    if (cmd.has_set_title()) {
        manager.set_window_title(window_id, cmd.set_title().title());
    } else if (cmd.has_move()) {
        manager.move_window_to(window_id, cmd.move().x(), cmd.move().y());
    } else if (cmd.has_resize()) {
        manager.resize_window(window_id, cmd.resize().width(), cmd.resize().height());
    } else if (cmd.has_set_bg_color()) {
        manager.set_window_background_color(
            window_id, cmd.set_bg_color().r(), cmd.set_bg_color().g(), cmd.set_bg_color().b());
    } else if (cmd.has_show_hide()) {
        manager.set_window_shown(window_id, cmd.show_hide().show());
    } else if (cmd.has_window_contents()) {
        manager.create_window_texture(window_id, main_window.renderer());
        uint8_t const* pixels = (uint8_t const*)cmd.window_contents().contents().data();
        manager.set_window_contents(window_id, pixels);
    } else if (cmd.has_set_window_class()) {
        auto window_class = (WindowManager::WindowClass)cmd.set_window_class().window_class();
        manager.set_window_class(window_id, window_class);
    } else if (cmd.has_query_display_size()) {
        send_display_size(server, id);
    }
}

class DragToMove
{
public:
    DragToMove(WindowManager& manager, Server& server)
        : m_manager(manager)
        , m_server(server)
    {
    }

    void handle_mouse_down(SDL_Point mouse_coords);
    void handle_mouse_move(SDL_Point mouse_coords);
    void handle_mouse_up(SDL_Point mouse_coords);

private:
    void send_new_position_to_window(WindowManager::Window const& window);

    WindowManager& m_manager;
    Server& m_server;

    std::optional<WindowManager::WindowId> m_dragging_window_id = std::nullopt;
    uint32_t m_last_cursor_x;
    uint32_t m_last_cursor_y;
};

void DragToMove::handle_mouse_down(SDL_Point mouse_coords)
{
    if (m_dragging_window_id)
        return;

    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);
    if (hit.area == WindowManager::HitArea::TitleBar) {
        m_dragging_window_id = hit.window_id;
        m_last_cursor_x = mouse_coords.x;
        m_last_cursor_y = mouse_coords.y;
    }
}

void DragToMove::handle_mouse_move(SDL_Point mouse_coords)
{
    if (!m_dragging_window_id)
        return;

    auto maybe_window = m_manager.get_window(*m_dragging_window_id);
    if (!maybe_window)
        return;
    auto const& window = *maybe_window;

    auto dx = mouse_coords.x - m_last_cursor_x;
    auto dy = mouse_coords.y - m_last_cursor_y;

    m_manager.move_window_to(*m_dragging_window_id, window.x + dx, window.y + dy);
    send_new_position_to_window(window);

    m_last_cursor_x = mouse_coords.x;
    m_last_cursor_y = mouse_coords.y;
}

void DragToMove::handle_mouse_up(SDL_Point mouse_coords)
{
    if (!m_dragging_window_id)
        return;

    auto maybe_window = m_manager.get_window(*m_dragging_window_id);
    if (!maybe_window)
        return;
    auto const& window = *maybe_window;

    m_manager.move_window_to(*m_dragging_window_id, window.x, window.y);
    send_new_position_to_window(window);

    m_dragging_window_id = std::nullopt;
}

void DragToMove::send_new_position_to_window(WindowManager::Window const& window)
{
    WindowingEvent event;
    auto* move_event = event.mutable_move();
    move_event->set_x(window.x);
    move_event->set_y(window.y);

    uint8_t buf[1024];
    size_t len = event.ByteSizeLong();
    event.SerializeToArray(buf, len);

    m_server.send_data(window.client_id, buf, len);
}

class ClickCloseButton
{
public:
    ClickCloseButton(WindowManager& manager, Server& server)
        : m_manager(manager)
        , m_server(server)
    {
    }

    void handle_mouse_up(SDL_Point mouse_coords);

private:
    void send_close_event(Server::ClientId id);

    WindowManager& m_manager;
    Server& m_server;
};

void ClickCloseButton::handle_mouse_up(SDL_Point mouse_coords)
{
    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);
    if (hit.area == WindowManager::HitArea::CloseButton) {
        auto const& window = *m_manager.get_window(hit.window_id);
        auto client_id = window.client_id;

        m_manager.destroy_window(hit.window_id);
        send_close_event(client_id);
    }
}

void ClickCloseButton::send_close_event(Server::ClientId id)
{
    fmt::print(stderr, "Sending close event to client {}\n", id);

    WindowingEvent event;
    event.set_allocated_close(new WindowingEvent_CloseEvent());

    uint8_t buf[1024];
    size_t len = event.ByteSizeLong();
    event.SerializeToArray(buf, len);

    m_server.send_data(id, buf, len);
}

class FocusWindow
{
public:
    FocusWindow(WindowManager& manager, Server& server)
        : m_manager(manager)
        , m_server(server)
    {
    }

    void handle_mouse_down(SDL_Point mouse_coords);

private:
    // void send_focus_event(Server::ClientId id);

    WindowManager& m_manager;
    Server& m_server;
};

void FocusWindow::handle_mouse_down(SDL_Point mouse_coords)
{
    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);
    if (hit.area == WindowManager::HitArea::None)
        return;

    m_manager.move_window_to_top(hit.window_id);
}

class ChangeMouseCursor
{
public:
    ChangeMouseCursor(WindowManager& manager, Server& server)
        : m_manager(manager)
        , m_server(server)
    {
    }

    void handle_mouse_move(SDL_Point mouse_coords);

private:
    WindowManager& m_manager;
    Server& m_server;
};

void ChangeMouseCursor::handle_mouse_move(SDL_Point mouse_coords)
{
    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);

    if (hit.area == WindowManager::HitArea::TitleBar)
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL));
    else if (hit.area == WindowManager::HitArea::CloseButton)
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND));
    else if (hit.area == WindowManager::HitArea::BottomBorder
        || hit.area == WindowManager::HitArea::TopBorder)
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENS));
    else if (hit.area == WindowManager::HitArea::LeftBorder
        || hit.area == WindowManager::HitArea::RightBorder)
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEWE));
    else if (hit.area == WindowManager::HitArea::BottomLeftCorner
        || hit.area == WindowManager::HitArea::TopRightCorner)
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW));
    else if (hit.area == WindowManager::HitArea::BottomRightCorner
        || hit.area == WindowManager::HitArea::TopLeftCorner)
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE));
    else if (hit.area == WindowManager::HitArea::Border)
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL));
    else if (hit.area == WindowManager::HitArea::ClientArea)
        // TODO: Get the cursor from the application as well
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW));
    else
        SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_NO));
}

class SendClientMouseEvents
{
public:
    SendClientMouseEvents(WindowManager& manager, Server& server)
        : m_manager(manager)
        , m_server(server)
    {
    }

    void handle_mouse_move(SDL_Point mouse_coords);
    void handle_mouse_down(SDL_Point mouse_coords, uint8_t sdl_button);
    void handle_mouse_up(SDL_Point mouse_coords, uint8_t sdl_button);

private:
    void send_mouse_click_event(Server::ClientId id, bool is_down, uint8_t sdl_button);
    void send_mouse_move_event(Server::ClientId id, uint32_t client_x, uint32_t client_y);

    WindowManager& m_manager;
    Server& m_server;
};

void SendClientMouseEvents::handle_mouse_move(SDL_Point mouse_coords)
{
    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);
    if (hit.area != WindowManager::HitArea::ClientArea)
        return;

    auto window = *m_manager.get_window(hit.window_id);
    if (window.class_ == WindowManager::WindowClass::Background)
        return;

    uint32_t client_x = mouse_coords.x - window.x;
    uint32_t client_y = mouse_coords.y - window.y;
    send_mouse_move_event(window.client_id, client_x, client_y);
}

void SendClientMouseEvents::handle_mouse_down(SDL_Point mouse_coords, uint8_t sdl_button)
{
    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);
    if (hit.area != WindowManager::HitArea::ClientArea)
        return;

    auto window = *m_manager.get_window(hit.window_id);
    if (window.class_ == WindowManager::WindowClass::Background)
        return;

    send_mouse_click_event(window.client_id, true, sdl_button);
}

void SendClientMouseEvents::handle_mouse_up(SDL_Point mouse_coords, uint8_t sdl_button)
{
    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);
    if (hit.area != WindowManager::HitArea::ClientArea)
        return;

    auto window = *m_manager.get_window(hit.window_id);
    if (window.class_ == WindowManager::WindowClass::Background)
        return;

    send_mouse_click_event(window.client_id, false, sdl_button);
}

void SendClientMouseEvents::send_mouse_click_event(
    Server::ClientId id, bool is_down, uint8_t sdl_button)
{
    fmt::print(stderr, "Sending mouse down event to client {}\n", id);

    WindowingEvent event;
    if (is_down) {
        auto* mouse_click_event = event.mutable_mouse_down();
        mouse_click_event->set_button(sdl_button);
    } else {
        auto* mouse_click_event = event.mutable_mouse_up();
        mouse_click_event->set_button(sdl_button);
    }

    uint8_t buf[1024];
    size_t len = event.ByteSizeLong();
    event.SerializeToArray(buf, len);

    m_server.send_data(id, buf, len);
}

void SendClientMouseEvents::send_mouse_move_event(
    Server::ClientId id, uint32_t client_x, uint32_t client_y)
{
    WindowingEvent event;
    auto* mouse_move_event = event.mutable_mouse_move();
    mouse_move_event->set_x(client_x);
    mouse_move_event->set_y(client_y);

    uint8_t buf[1024];
    size_t len = event.ByteSizeLong();
    event.SerializeToArray(buf, len);

    m_server.send_data(id, buf, len);
}

enum class ResizeType {
    None,
    Left,
    Right,
    Top,
    Bottom,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight
};

class ResizeWindow
{
public:
    ResizeWindow(WindowManager& manager, Server& server)
        : m_manager(manager)
        , m_server(server)
    {
    }

    void handle_mouse_down(SDL_Point mouse_coords);
    void handle_mouse_move(SDL_Point mouse_coords);
    void handle_mouse_up(SDL_Point mouse_coords);

private:
    void calculate_new_resize_rect(SDL_Point mouse_coords);
    void send_new_position_and_size_to_window(WindowManager::Window const& window);

    WindowManager& m_manager;
    Server& m_server;

    std::optional<WindowManager::WindowId> m_window_id = std::nullopt;
    ResizeType m_resize_type = ResizeType::None;

    SDL_Rect m_current_resize_rect;
};

void ResizeWindow::handle_mouse_down(SDL_Point mouse_coords)
{
    auto hit = m_manager.hit_test(mouse_coords.x, mouse_coords.y);
    if (hit.area == WindowManager::HitArea::Border
        || hit.area == WindowManager::HitArea::BottomBorder
        || hit.area == WindowManager::HitArea::TopBorder
        || hit.area == WindowManager::HitArea::LeftBorder
        || hit.area == WindowManager::HitArea::RightBorder
        || hit.area == WindowManager::HitArea::BottomLeftCorner
        || hit.area == WindowManager::HitArea::BottomRightCorner
        || hit.area == WindowManager::HitArea::TopLeftCorner
        || hit.area == WindowManager::HitArea::TopRightCorner) {
        m_window_id = hit.window_id;
        auto window = *m_manager.get_window(*m_window_id);
        m_current_resize_rect = window.window_outer_rect();

        switch (hit.area) {
        case WindowManager::HitArea::Border:
            m_resize_type = ResizeType::None;
            break;
        case WindowManager::HitArea::BottomBorder:
            m_resize_type = ResizeType::Bottom;
            break;
        case WindowManager::HitArea::TopBorder:
            m_resize_type = ResizeType::Top;
            break;
        case WindowManager::HitArea::LeftBorder:
            m_resize_type = ResizeType::Left;
            break;
        case WindowManager::HitArea::RightBorder:
            m_resize_type = ResizeType::Right;
            break;
        case WindowManager::HitArea::BottomLeftCorner:
            m_resize_type = ResizeType::BottomLeft;
            break;
        case WindowManager::HitArea::BottomRightCorner:
            m_resize_type = ResizeType::BottomRight;
            break;
        case WindowManager::HitArea::TopLeftCorner:
            m_resize_type = ResizeType::TopLeft;
            break;
        case WindowManager::HitArea::TopRightCorner:
            m_resize_type = ResizeType::TopRight;
            break;
        default:
            m_resize_type = ResizeType::None;
            break;
        }
    }
}

void ResizeWindow::calculate_new_resize_rect(SDL_Point mouse_coords)
{
    if (!m_window_id)
        return;

    auto maybe_window = m_manager.get_window(*m_window_id);
    if (!maybe_window)
        return;
    auto const& window = *maybe_window;

    switch (m_resize_type) {
    case ResizeType::None:
        break;
    case ResizeType::Bottom:
        m_current_resize_rect.h = mouse_coords.y - window.y;
        break;
    case ResizeType::Top:
        m_current_resize_rect.y = mouse_coords.y;
        m_current_resize_rect.h = window.y + window.h - mouse_coords.y;
        break;
    case ResizeType::Left:
        m_current_resize_rect.x = mouse_coords.x;
        m_current_resize_rect.w = window.x + window.w - mouse_coords.x;
        break;
    case ResizeType::Right:
        m_current_resize_rect.w = mouse_coords.x - window.x;
        break;
    case ResizeType::BottomLeft:
        m_current_resize_rect.x = mouse_coords.x;
        m_current_resize_rect.w = window.x + window.w - mouse_coords.x;
        m_current_resize_rect.h = mouse_coords.y - window.y;
        break;
    case ResizeType::BottomRight:
        m_current_resize_rect.w = mouse_coords.x - window.x;
        m_current_resize_rect.h = mouse_coords.y - window.y;
        break;
    case ResizeType::TopLeft:
        m_current_resize_rect.x = mouse_coords.x;
        m_current_resize_rect.w = window.x + window.w - mouse_coords.x;
        m_current_resize_rect.y = mouse_coords.y;
        m_current_resize_rect.h = window.y + window.h - mouse_coords.y;
        break;
    case ResizeType::TopRight:
        m_current_resize_rect.w = mouse_coords.x - window.x;
        m_current_resize_rect.y = mouse_coords.y;
        m_current_resize_rect.h = window.y + window.h - mouse_coords.y;
        break;
    }
}

void ResizeWindow::handle_mouse_move(SDL_Point mouse_coords)
{
    if (!m_window_id)
        return;

    auto maybe_window = m_manager.get_window(*m_window_id);
    if (!maybe_window)
        return;
    auto const& window = *maybe_window;

    calculate_new_resize_rect(mouse_coords);
    m_manager.move_window_to(*m_window_id, m_current_resize_rect.x, m_current_resize_rect.y);
    m_manager.resize_window(*m_window_id, m_current_resize_rect.w, m_current_resize_rect.h);
    send_new_position_and_size_to_window(window);
}

void ResizeWindow::handle_mouse_up(SDL_Point mouse_coords)
{
    if (!m_window_id)
        return;

    auto maybe_window = m_manager.get_window(*m_window_id);
    if (!maybe_window)
        return;
    auto const& window = *maybe_window;

    m_window_id = std::nullopt;
}

void ResizeWindow::send_new_position_and_size_to_window(WindowManager::Window const& window)
{
    WindowingEvent event;
    auto* resize_event = event.mutable_resize();
    resize_event->set_width(m_current_resize_rect.w);
    resize_event->set_height(m_current_resize_rect.h);

    uint8_t buf[1024];
    size_t len = event.ByteSizeLong();
    event.SerializeToArray(buf, len);

    m_server.send_data(window.client_id, buf, len);

    WindowingEvent event2;
    auto* move_event = event2.mutable_move();
    move_event->set_x(m_current_resize_rect.x);
    move_event->set_y(m_current_resize_rect.y);

    len = event2.ByteSizeLong();
    event2.SerializeToArray(buf, len);

    m_server.send_data(window.client_id, buf, len);
}

void send_ping_event(Server::ClientId id, Server& server)
{
    fmt::print(stderr, "Sending ping event to client {}\n", id);

    WindowingEvent event;
    event.set_allocated_ping(new WindowingEvent_PingEvent());

    uint8_t buf[1024];
    size_t len = event.ByteSizeLong();
    event.SerializeToArray(buf, len);

    server.send_data(id, buf, len);
}

Server* signal_handler_only_server = nullptr;

int main(int argc, char** argv)
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    Submodule::Stuff submodule_stuff;
    submodule_stuff.do_stuff();

    SDLWindow display_window("Windowing Thingy Display", { 800, 600 });

    FontLoader font_loader;
    auto maybe_font_path = font_loader.locate_font(":family=sans-serif");
    fmt::print(stderr, "Font path: {}\n", maybe_font_path.value_or("not found"));

    TextRenderer text_renderer;
    if (maybe_font_path)
        text_renderer.load_font(display_window.renderer(), *maybe_font_path, 16);

    WindowManager window_manager(text_renderer);
    Server server(5678);

    // behaviors
    DragToMove drag_to_move(window_manager, server);
    ClickCloseButton click_close_button(window_manager, server);
    FocusWindow focus_window(window_manager, server);
    ChangeMouseCursor change_mouse_cursor(window_manager, server);
    SendClientMouseEvents send_client_mouse_events(window_manager, server);
    ResizeWindow resize_window(window_manager, server);

    server.on_new_connection([&](Server::ClientId id) {
        cout << "New connection" << endl;
        window_manager.create_window(id);
        send_ping_event(id, server);
    });
    server.on_data_received([&](Server::ClientId id, uint8_t* buf, size_t len) {
        cout << "Received " << len << " bytes" << endl;
        handle_windowing_command(window_manager, display_window, server, id, buf, len);
    });
    server.on_connection_closed([&](Server::ClientId id) {
        cout << "Connection closed" << endl;
        auto window_id = window_manager.window_id_from_client_id(id);
        window_manager.destroy_window(window_id);
    });
    server.run();

    signal_handler_only_server = &server;
    struct sigaction sigint_handler;
    sigint_handler.sa_flags = SA_SIGINFO;
    sigint_handler.sa_sigaction = [](int, siginfo_t*, void* ctx) {
        std::cout << "SIGINT received. Quitting" << std::endl;
        if (signal_handler_only_server)
            signal_handler_only_server->shutdown();
        exit(0);
    };
    sigaction(SIGINT, &sigint_handler, nullptr);

    SDLEventLoop event_loop;
    event_loop.register_event(SDL_QUIT, [&](SDL_Event evt) {
        std::cout << "Display closed. Quitting" << std::endl;
        server.shutdown();
        event_loop.stop();
    });
    event_loop.register_event(SDL_MOUSEBUTTONDOWN, [&](SDL_Event evt) {
        drag_to_move.handle_mouse_down({ evt.button.x, evt.button.y });
        focus_window.handle_mouse_down({ evt.button.x, evt.button.y });
        send_client_mouse_events.handle_mouse_down(
            { evt.button.x, evt.button.y }, evt.button.button);
        resize_window.handle_mouse_down({ evt.button.x, evt.button.y });
    });
    event_loop.register_event(SDL_MOUSEBUTTONUP, [&](SDL_Event evt) {
        drag_to_move.handle_mouse_up({ evt.button.x, evt.button.y });
        click_close_button.handle_mouse_up({ evt.button.x, evt.button.y });
        send_client_mouse_events.handle_mouse_up({ evt.button.x, evt.button.y }, evt.button.button);
        resize_window.handle_mouse_up({ evt.button.x, evt.button.y });
    });

    event_loop.register_event(SDL_MOUSEMOTION, [&](SDL_Event evt) {
        drag_to_move.handle_mouse_move({ evt.button.x, evt.button.y });
        change_mouse_cursor.handle_mouse_move({ evt.button.x, evt.button.y });
        send_client_mouse_events.handle_mouse_move({ evt.button.x, evt.button.y });
        resize_window.handle_mouse_move({ evt.button.x, evt.button.y });

        auto hit = window_manager.hit_test(evt.button.x, evt.button.y);
        if (hit.area == WindowManager::HitArea::Border) {
            SDL_SetCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL));
        }
    });

    auto renderer = display_window.renderer();

    event_loop.run([&](uint64_t ticks) {
        text_renderer.tick(ticks);
        window_manager.tick(ticks);

        // TODO: Try not to redraw the whole thing every frame

        SDL_SetRenderDrawColor(renderer, 125, 0, 125, 255);
        SDL_Rect screen_rect = { 0, 0, 800, 600 };
        SDL_RenderFillRect(renderer, &screen_rect);

        for (auto it = window_manager.bottom(); it != window_manager.top_end(); ++it) {
            auto const& window = **it;
            window_manager.draw_window_onto(window, renderer);
        }

        display_window.draw();
    });
}
