#include "SDLWindow.h"
using namespace std;

#include <fmt/core.h>
#include <vendor/stb_image.h>

#define SDL_MUST(expr)                                                                             \
    do {                                                                                           \
        int rc = (expr);                                                                           \
        if (rc != 0) {                                                                             \
            fmt::print(stderr, "{} returned {}: {}\n", #expr, rc, SDL_GetError());                 \
            exit(1);                                                                               \
        }                                                                                          \
    } while (0)

SDLWindow::SDLWindow(string_view title, SDL_Point size)
    : m_window_size(size)
{
    SDL_MUST(SDL_Init(SDL_INIT_VIDEO));

#ifdef USE_OPENGL
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE));

    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24));
#endif

    string title_copy(title);
    window = SDL_CreateWindow(title_copy.c_str(), 0, 0, size.x, size.y,
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    assert(window);
    m_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    assert(m_renderer);

#ifdef USE_OPENGL
    ctx = SDL_GL_CreateContext(window);
    SDL_MUST(SDL_GL_MakeCurrent(window, ctx));

    SDL_MUST(SDL_GL_SetSwapInterval(1));
#endif
}

SDLWindow::~SDLWindow()
{
    if (m_window_icon)
        SDL_FreeSurface(m_window_icon);
#ifdef USE_OPENGL
    SDL_GL_DeleteContext(ctx);
#endif
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void SDLWindow::draw()
{
    assert(window);

#ifdef USE_OPENGL
    SDL_GL_SwapWindow(window);
#else
    SDL_RenderPresent(m_renderer);
#endif
}

void SDLWindow::set_title(string_view title)
{
    assert(window);

    string title_copy(title);
    SDL_SetWindowTitle(window, title_copy.c_str());
}

void SDLEventLoop::run(function<void(uint64_t)> body)
{
    m_running = true;
    while (m_running) {
        uint64_t ticks = SDL_GetTicks64();

        SDL_Event evt;
        while (SDL_PollEvent(&evt)) {
            if (evt.type == SDL_QUIT)
                m_running = false;

            auto it = m_event_cbs.find((SDL_EventType)evt.type);
            if (it != m_event_cbs.end())
                it->second(evt);
        }
        if (!m_running)
            break;

        body(ticks);
    }
}

void SDLEventLoop::register_event(SDL_EventType type, function<void(SDL_Event)> callback)
{
    assert(m_event_cbs.find(type) == m_event_cbs.end());
    m_event_cbs.insert({ type, callback });
}

void SDLEventLoop::stop() { m_running = false; }

void SDLWindow::set_icon(string const& filename)
{
    assert(!m_window_icon);

    int w, h, c;
    void* data = stbi_load(filename.c_str(), &w, &h, &c, 4);
    assert(c == 4);

    m_window_icon = SDL_CreateRGBSurfaceWithFormat(0, w, h, c * 8, SDL_PIXELFORMAT_ARGB8888);
    assert(m_window_icon);

    SDL_LockSurface(m_window_icon);
    memcpy(m_window_icon->pixels, data, w * h * 4);
    SDL_UnlockSurface(m_window_icon);

    SDL_SetWindowIcon(window, m_window_icon);
    stbi_image_free(data);
}
